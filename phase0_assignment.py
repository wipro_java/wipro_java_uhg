
#program 1
def assignment(A,B,N):
    A_time , A_page = A[0] , A[1]
    B_time , B_page = B[0] , B[1]

    total_page = A_page + B_page
    total_time = A_time + B_time

    return (total_time * N) // total_page

print(assignment([6,32],[5,40],110))

################################################

#program 2a

def gcd(a,b):
    if b == 0:
        return a
    else:
        return gcd(b, a%b)
        
print(gcd(43,gcd(91,183)))

# 2b

def function(nums):
    result = []

    for i in range(len(nums)):
        if nums[i] % 2 == 1:
            result.append(nums[i])

    result.sort()

    return result


print(function(list(map(int,input().strip().split()))))

##################################################

# program 3


def train_problem(x,y):
    length_ = (700 / 1000) * 2

    total_speed = x + y

    total_time = length_ / total_speed

    return total_time

print(train_problem(int(input()),int(input())))

######################################################

#program 4

def students(N):

    students = (N * 90) // 100

    return (students * 2) // 3

print(students(int(input())))

#####################################################

#program 5 

def students(student):
    count = 0
    
    for key,values in student.items():
        flag = 0

        for k in student[key]:
            if k < 40:
                flag = 1
                break

        if flag == 0:
            count += 1

    return count / len(student)

print(students({'a':[40,35,5],'b':[56,55,75],'c':[46,65,57]}))


######################################################


#program 6

def smallest_integer(nums):

    hash_table = {}
    result = []

    for i in range(len(nums)):
        hash_table[nums[i]] = 0

    for i in range(len(nums)):

        for j in range(len(nums)):
            sum = nums[i] + nums[j]

            if i != j and  sum in hash_table.keys():
                hash_table[sum] = -1

    for i in hash_table.keys():
        if hash_table[i] == 0:
            result.append(i)

    return min(result)

print(smallest_integer([5,3,2,6,4]))


#####################################################

#program 7

def sum_(N):

    sum = 0

    for i in range(1,N):
        if i % 2 == 0:
            sum += i

    return sum


print(sum_(int(input()))

##########################################

#program 8

def vowels(s):
    vowel = {'a','e','i','o','u'}
    flag = 0

    for i in range(len(s)-1):
        if s[i] in vowel and s[i+1] in vowel :
            print('consecutive vowels present')
            flag = 1
            break

    if flag == 0:
        print('no consecutive vowels')


print(vowels(input()))

#############################################

#program 9


def isHappy(n: int) -> bool:
    sum = happy(n)
    n = sum
    t = 10
        
    while t != 0 :
        if sum != 1:
            sum = happy(n)
            n = sum
        else:
            return True
            t-=1
            
    return False
    
def happy(n):
    sum = 0
    while n > 0:
        x = n % 10
        sum = sum + (x*x)
        n = int(n / 10)
    
    return sum

if isHappy(int(input())):
    print('happy number')
else:
    print('unhappy number')

##########################################

#program 11

import math

def roots(a,b,c):

    determinant = ((b*b) - 4 * a * c)

    if determinant < 0:
        return -1
    elif determinant == 0:
        return 0
    else:

        root1 = ((-1 * b) + math.sqrt(determinant)) / (2 * a)
        root 2 =  ((-1 * b) - math.sqrt(determinant)) / (2 * a)

    return (root1,root2)

print(roots(int(input()),int(input()),int(input())))


##########################################

# program 12

def tax(emp_id,income):

    if income <= 150000:
        return 0
    elif income > 150000 and income <= 300000:
        return (income * 10) / 100
    elif income > 300000 and income <= 500000:
        return (income * 15) / 100
    else:
        return (income * 25) / 100

print(tax(input(),int(input())))

#############################################

#program 13


def combination(coins,target):
    coins.sort()
    result = []
    combinations = []
    new_result = []

    solution(coins,combinations,result,target,i=0)

    for ele in result:
        if ele not in new_result:
            new_result.append(ele)

    return new_result

def solution(coins,combinations,result,target,i):
    if sum(combinations) == target:
        combinations.sort()
        result.append(combinations)
        return

    if sum(combinations) > target:
        return

    while i < len(coins):
        solution(coins,combinations+[coins[i]],result,target,i+1)
        i+=1


print(combination([1,1,1,1,1,10,10,15],20))

#####################################################
